# HTML_Screensaver
一个基于Electron的屏保程序，有如下特性：
- 支持自定义扩展HTML屏保特效
- 多屏合并为一屏展示屏保特效
- 多个屏幕，每个屏幕指定不同特效

------ 

## 使用说明：
屏保扩展分为两部分
- 本程序只支持64位操作系统，暂支持32位系统
- 由于有些权限问题，所以最好将程序包解压放在如下位置： **C:\Windows\SysWOW64\\**，或者**C:\Windows\System32\\**
- 解压后，右击 **HTMLScreenSaver.scr**，然后选择安装就行了
- 暂时未增加配置功能，待空了开发

- ScreenEffects文件夹
> ScreenEffects文件夹里面的每一个文件夹表示一个屏保特效HTML源代码。扩展特效时，只需要在ScreenEffects文件夹下新建一个文件夹，然后将写好的HTML屏保特效代码放入新建的文件夹就可以了。（记得：web页面名称一定是叫 index.html 哦！eg: ScreenEffects\\particleSpiral\\**index.html**）

- 安装使用：
> 下载.zip压缩包后，找到HTMLScreenSaver.scr文件，鼠标右键菜单中选择安装即可


## 屏保程序开发流程及说明：
[参见此示例工程](https://www.codeproject.com/articles/4809/how-to-develop-a-screen-saver-in-c#:~:text=%20The%20code%20explained%20%201%20Step%201%3A,the%20screensaver%2C%20you%20call%20the%20System.Windows.Forms.Application.Run...%20More%20)

## 问题集：
- Win10中屏保程序无法使用问题：[https://windowsreport.com/screen-saver-not-working-windows-10/](https://windowsreport.com/screen-saver-not-working-windows-10/)
- 将Electron程序打包为独立程序：[https://www.boxedapp.com/boxedapppacker/usecases/pack_electron_app_into_single_exe.html](https://www.boxedapp.com/boxedapppacker/usecases/pack_electron_app_into_single_exe.html)